<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(array('prefix' => '/v1'), function()
{
	Route::post('/auth/app', array('uses' => 'Api\V1\AuthController@authenticate'));

	Route::post('/promocodes/generate', array('uses' => 'Api\V1\PromocodesController@post'))->middleware('auth.api.app');
	Route::put('/promocodes/activate', array('uses' => 'Api\V1\PromocodesController@put'))->middleware('auth.api.app');
	Route::get('/promocodes/verify', array('uses' => 'Api\V1\PromocodesController@get'))->middleware('auth.api.app');

	Route::post('/campaigns', array('uses' => 'Api\V1\CampaignsController@post'))->middleware('auth.api.app');
});