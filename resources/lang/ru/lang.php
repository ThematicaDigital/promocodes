<?php

return [
    'sleeping_owl::lang.message.destroyed' => '<i class="fa fa-check fa-lg"></i> Запись успешно удалена',
    'sleeping_owl::lang.table.delete-confirm' => 'Отменить',
]