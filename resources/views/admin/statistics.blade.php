<div class="panel panel-default">
	<div class="panel-heading">
		<form id="filters-statistics" method="GET" action="{{ route('chars-ajax') }}">
			<div class="row">
				<div class="col-md-2">
					{!! $applications !!}
				</div>
				<div class="col-md-2">
					{!! $campaigns !!}
				</div>
				<div class="col-md-2">
	            	{!! $start_date !!}
				</div>
				<div class="col-md-2">
	            	{!! $end_date !!}
				</div>
				<div class="col-md-2">
					{!! $parametrs !!}
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="control-label" style="width:100%">&nbsp;</label>
						<button class="btn btn-primary">
							<i class="fa fa-filter"></i> Фильтровать
						</button>
	            	</div>
				</div>
			</div>
		</form>
	</div>
	<div class="panel">
		<div id="chars"></div>
		{!! $lava->render('LineChart', 'ActivationByDate', 'chars') !!}
	</div>
</div>