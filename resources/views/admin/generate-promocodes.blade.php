<div id="generate-promocodes">
	<!-- EXPORT -->
	<div class="pull-right" style="margin-left:10px">
		<a href="{{ route('export-codes', $campaign_id) }}" class="btn btn-primary export-codes" target="_blank">
			<i class="fa fa-download"></i> Экспортировать
		</a>
	</div>

	<!-- IMPORT -->
	<div class="pull-right" style="margin-left:10px">
		<button type="button" class="btn btn-primary import-codes" data-campaign_id="{{ $campaign_id }}">
			<i class="fa fa-upload"></i> Импортировать
		</button>
		<input name="upload-codes" type="file" id="upload-codes" style="display:none" />
	</div>

	<!-- GENERATION -->
	<div class="pull-right" style="margin-left:10px">
		<div class="pull-right" style="margin-left:10px">
			<button type="button" class="btn btn-primary generate-codes" data-campaign_id="{{ $campaign_id }}">
				<i class="fa fa-cube"></i> Генерировать
			</button>
		</div>
		<div class="pull-right">
			<input type="number" name="count-generate-codes" value="" min="1" placeholder="Количество промокодов" class="form-control"/>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="clearfix"></div>
	<input type="hidden" name="campaign_id" value="{{ $campaign_id }}" />
</div>

<!-- fa fa-spinner fa-spin -->