<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Codes extends Model
{
	use SoftDeletes;
    use ValidatingTrait;

	/**
	 * Status activation
	 * @var integer
	 */
	const ACTIVATION = 1;

	/**
	 * Status no activation
	 * @var integer
	 */
	const NO_ACTIVATION = 2;

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'code' => 'required|string',
        'generation_date' => 'date|date_format:Y-m-d|nullable',
        'activation_date' => 'date|date_format:Y-m-d|nullable',
        'status' => 'required|integer',
        'campaign_id' => 'required|integer',
    ];

    /**
     * Get campaign
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function campaign()
    {
        return $this->belongsTo(\App\Models\Campaigns::class);
    }

    /**
     * Scope codes with campaign
     * @param \Illuminate\Database\Query
     * @param integer
     * @return \Illuminate\Database\Query
     */
    public function scopeWithCampaign($query, $campaign_id)
    {
        return $query->where('campaign_id', $campaign_id);
    }

    /**
     * Scope count activation codes by date
     * @param \Illuminate\Database\Query
     * @param integer
     * @return \Illuminate\Database\Query
     */
    public function scopeCountActivationByDate($query)
    {
        return $query->where('status', self::ACTIVATION)->gorupBy('activation_date');
    }
}
