<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use \Firebase\JWT\JWT;

class Applications extends Model
{
	use SoftDeletes;
    use ValidatingTrait;

    /**
     * Platform Android
     * @var integer
     */
    const PLATFORM_ANDROID = 1;

    /**
     * Platform iOS
     * @var integer
     */
    const PLATFORM_IOS = 2;

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string'
    ];

    /**
     * Get campaigns
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function campaigns()
    {
        return $this->belongsToMany(\App\Models\Campaigns::class, 'applications_campaigns', 'application_id', 'campaign_id');
    }

    /**
     * Encode string platform to integer
     *
     * @param string $platform
     * @return integer|boolean
     */
    public static function encodePlatform($platform)
    {
        if (strtolower($platform) == 'android') {
            return self::PLATFORM_ANDROID;
        } else if (strtolower($platform) == 'ios') {
            return self::PLATFORM_IOS;
        }
        throw new \Exception('Platform not found', 404);
        return false;
    }

    /**
     * Decode integer platform to string
     *
     * @param integer $integer
     * @return string|boolean
     */
    public static function decodePlatform($platform)
    {
        if ($platform == self::PLATFORM_ANDROID) {
            return 'android';
        } else if ($platform == self::PLATFORM_IOS) {
            return 'ios';
        }
        return false;
    }

    /**
     * Generate API token
     *
     * @return string
     */
    public function generateAuthToken()
    {
        $jwt = JWT::encode([
            'iss' => ENV('API_AUTH_ISS'),
            'sub' => $this->api_key,
            'iat' => time(),
            'exp' => time() + ENV('API_AUTH_EXP'),
        ], ENV('API_AUTH_KEY'));
        return $jwt;
    }
}
