<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Campaigns extends Model
{
	use SoftDeletes;
    use ValidatingTrait;

    /**
     * Fillable fields
     * @var array
     */
    protected $fillable = [
        'title',
        'consumable',
        'start_date',
        'end_date',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
        'start_date' => 'date|date_format:Y-m-d|nullable',
        'end_date' => 'date|date_format:Y-m-d|nullable',
    ];

    /**
     * Get codes
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function codes()
    {
        return $this->hasMany(\App\Models\Codes::class, 'campaign_id');
    }

    /**
     * Get applications
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function applications()
    {
        return $this->belongsToMany(\App\Models\Applications::class, 'applications_campaigns', 'campaign_id', 'application_id');
    }

    /**
     * Get rewards
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rewards()
    {
        return $this->belongsToMany(\App\Models\Rewards::class, 'campaigns_rewards', 'campaign_id', 'reward_id');
    }
}
