<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Rewards extends Model
{
	use SoftDeletes;
    use ValidatingTrait;

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'reward' => 'required|string',
        'title' => 'required|string',
    ];

    /**
     * Get campaigns
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function campaigns()
    {
        return $this->belongsToMany(\App\Models\Campaigns::class, 'campaigns_rewards', 'reward_id', 'campaign_id');
    }
}
