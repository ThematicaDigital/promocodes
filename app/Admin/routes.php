<?php

Route::get('', ['as' => 'admin.dashboard', function () {
	$content = 'Define your dashboard here.';
	return AdminSection::view($content, 'Dashboard');
}]);

Route::get('statistics', '\App\Http\Controllers\AdminController@statistics')->name('statistics');
Route::get('chars', '\App\Http\Controllers\AdminController@chars')->name('chars-ajax');
Route::post('promocodes/generate-codes', '\App\Http\Controllers\AdminController@generateCodes')->name('generate-codes');
Route::get('promocodes/export-codes/{campaign_id}', '\App\Http\Controllers\AdminController@exportCodes')->name('export-codes');
Route::post('promocodes/import-codes/{campaign_id}', '\App\Http\Controllers\AdminController@importCodes')->name('import-codes');