<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use Meta;

/**
 * Class Applications
 *
 * @property \App\Models\Applications $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Applications extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Приложения';

    /**
     * @var string
     */
    protected $alias = 'applications';

    /**
     * Initialize section
     */
    public function initialize()
    {
        return $this->addToNavigation()->setIcon('fa fa-android');
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->with('campaigns')
            ->setOrder([[0, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('title', 'Название'),
                AdminColumn::text('api_key', 'API KEY'),
                AdminColumn::text('api_secret', 'API SECRET'),
                AdminColumn::text('bundle_ios', 'Bundle ID iOS'),
                AdminColumn::text('bundle_android', 'Bundle ID Android'),
                AdminColumn::count('campaigns', 'Кол-во кампаний')->setOrderable(false)
            )->paginate(25);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        Meta::addJs('admin-applications', asset('packages/sleepingowl/custom/applications.js'), 'admin-default');
        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Название')->required()->addValidationRule('string'),
            AdminFormElement::textaddon('api_key', 'API KEY')
                ->required()
                ->addValidationRule('string')
                ->setAddon('<i class="fa fa-refresh" title="Generate" style="cursor:pointer" rel="generate"></i>')
                ->placeAfter(),
            AdminFormElement::textaddon('api_secret', 'API SECRET')
                ->required()
                ->addValidationRule('string')
                ->setAddon('<i class="fa fa-refresh" title="Generate" style="cursor:pointer" rel="generate"></i>')
                ->placeAfter(),
            AdminFormElement::text('bundle_ios', 'Bundle ID iOS'),
            AdminFormElement::text('bundle_android', 'Bundle ID Android')
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
