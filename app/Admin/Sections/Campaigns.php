<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;

use App\Models\Applications;
use App\Models\Codes;
use App\Models\Rewards;

/**
 * Class Campaigns
 *
 * @property \App\Models\Campaigns $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Campaigns extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Кампании';

    /**
     * @var string
     */
    protected $alias = 'campaigns';

    /**
     * Initialize section
     */
    public function initialize()
    {
        return $this->addToNavigation()->setIcon('fa fa-list');
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->with('rewards', 'applications')
            ->setApply(function ($query) {
                $query->select(\DB::raw('campaigns.*, (SELECT COUNT(*) FROM codes WHERE codes.campaign_id = campaigns.id) AS count'));
            })
            ->setOrder([[0, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('title', 'Название'),
                AdminColumn::custom('Consumable', function ($instance) {
                    if ($instance->consumable) {
                        return '<span class="badge" style="background:#28a745">Да</span>';
                    } else {
                        return '<span class="badge badge-secondary">Нет</span>';
                    }
                })->setWidth(150)->setOrderable(function($query, $order){
                    return $query->orderBy('consumable', $order);
                }),
                AdminColumn::datetime('start_date', 'Дата начала')->setFormat('Y-m-d'),
                AdminColumn::datetime('end_date', 'Дата окончания')->setFormat('Y-m-d'),
                AdminColumn::lists('applications.title', 'Приложения'),
                AdminColumn::text('count', 'Кол-во промокодов'),
                // AdminColumn::count('codes', 'Кол-во промокодов')->setOrderable(false),
                AdminColumn::lists('rewards.title', 'Награды')
            )->paginate(25);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        Meta::addJs('admin-campaigns', asset('packages/sleepingowl/custom/campaigns.js'), 'admin-default');

        $promocodes = AdminSection::getModel(Codes::class)->fireDisplay(['params' => ['withCampaign' => $id]]);
        $promocodes->getScopes()->push(['withCampaign', $id]);
        $promocodes->setParameter('campaign_id', $id);
        $promocodes->getActions()->setView(view(
            'admin.generate-promocodes',
            ['campaign_id' => $id]
        ))->setPlacement('panel.buttons');

        $tabs = AdminDisplay::tabbed([
            'Настройки кампании' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::text('title', 'Название')->required()->addValidationRule('string'),
                AdminFormElement::checkbox('consumable', 'Consumable'),
                AdminFormElement::date('start_date', 'Дата начала')->setFormat('Y-m-d')->setPickerFormat('Y-m-d'),
                AdminFormElement::date('end_date', 'Дата окончания')->setFormat('Y-m-d')->setPickerFormat('Y-m-d'),
                AdminFormElement::multiselect('applications', 'Приложения', Applications::class)->setDisplay('title')->taggable(),
                AdminFormElement::multiselect('rewards', 'Награды', Rewards::class)->setDisplay('title'),
            ]),
            'Промокоды' => new \SleepingOwl\Admin\Form\FormElements([
                $promocodes
            ]),
        ]);
        return AdminForm::panel()->addElement($tabs);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $tabs = AdminDisplay::tabbed([
            'Настройки кампании' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::text('title', 'Название')->required()->addValidationRule('string'),
                AdminFormElement::checkbox('consumable', 'Consumable'),
                AdminFormElement::date('start_date', 'Дата начала')->setFormat('Y-m-d')->setPickerFormat('Y-m-d'),
                AdminFormElement::date('end_date', 'Дата окончания')->setFormat('Y-m-d')->setPickerFormat('Y-m-d'),
                AdminFormElement::multiselect('applications', 'Приложения', Applications::class)->setDisplay('title')->taggable(),
                AdminFormElement::multiselect('rewards', 'Награды', Rewards::class)->setDisplay('title'),
            ]),
        ]);
        return AdminForm::panel()->addElement($tabs);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
