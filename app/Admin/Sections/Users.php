<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;

/**
 * Class Users
 *
 * @property \App\Models\Users $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Users extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Пользователи';

    /**
     * @var string
     */
    protected $alias = 'users';

    /**
     * Initialize section
     */
    public function initialize()
    {
        return $this->addToNavigation()->setIcon('fa fa-user');
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[0, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Имя'),
                AdminColumn::email('email', 'E-mail')
            )->paginate(25);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Имя')
                ->required()
                ->addValidationRule('string'),
            AdminFormElement::text('email', 'E-mail')
                ->required()
                ->addValidationRule('email', 'Неккоректный e-mail адрес'),
            AdminFormElement::password('password', 'Пароль')
                ->hashWithBcrypt()
                ->required()
                ->addValidationRule('string')
                ->addValidationRule('min:6', 'Пароль должен содержать минимум 6 символов'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Имя')
                ->required()
                ->addValidationRule('string'),
            AdminFormElement::text('email', 'E-mail')
                ->required()
                ->addValidationRule('unique:users,email')
                ->addValidationRule('email', 'Неккоректный e-mail адрес'),
            AdminFormElement::password('password', 'Пароль')
                ->hashWithBcrypt()
                ->required()
                ->addValidationRule('string')
                ->addValidationRule('min:6', 'Пароль должен содержать минимум 6 символов'),
        ]);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
