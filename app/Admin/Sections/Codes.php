<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;

/**
 * Class Codes
 *
 * @property \App\Models\Codes $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Codes extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Промокоды';

    /**
     * @var string
     */
    protected $alias = 'promocodes';

    /**
     * Initialize section
     */
    public function initialize()
    {
        return $this->addToNavigation()->setIcon('fa fa-question');
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay($params = null)
    {
        $display = AdminDisplay::datatablesAsync()
            ->setOrder([[0, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('code', 'Промокод'),
                AdminColumn::datetime('generation_date', 'Дата генерации')->setFormat('Y-m-d'),
                AdminColumn::datetime('activation_date', 'Дата активации')->setFormat('Y-m-d'),
                AdminColumn::custom('Статус', function ($instance) {
                    if ($instance->status == \App\Models\Codes::ACTIVATION) {
                        return '<span class="badge" style="background:#28a745">Активирован</span>';
                    } else {
                        return '<span class="badge badge-secondary">Не активирован</span>';
                    }
                })->setWidth(150)->setOrderable(function($query, $order){
                    return $query->orderBy('status', $order);
                }),
                AdminColumn::relatedLink('campaign.title', 'Кампания')
            )->paginate(25);

        if ($params) {
            if (!empty($params['withCampaign'])) {
                $display->setScopes(['withCampaign' , $params['withCampaign']]);
            }
        }

        return $display;
    }

    // /**
    //  * @return FormInterface
    //  */
    // public function onCreate()
    // {
    //     return $this->onEdit(null);
    // }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
