<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\Models\Applications::class => 'App\Admin\Sections\Applications',
        \App\Models\Campaigns::class => 'App\Admin\Sections\Campaigns',
        \App\Models\Codes::class => 'App\Admin\Sections\Codes',
        \App\Models\Rewards::class => 'App\Admin\Sections\Rewards',
        \App\Models\Users::class => 'App\Admin\Sections\Users',
        // \App\Models\Statistics::class => 'App\Admin\Sections\Statistics',
    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
