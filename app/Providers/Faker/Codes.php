<?php

namespace App\Providers\Faker;

class Codes extends \Faker\Provider\Base
{
	// const CHARS = "QWRYUISDFGHJLZXVN1234567890";
	const CHARS = ['Q','W','R','Y','U','I','S','D','F','G','H','J','L','Z','X','V','N','1','2','3','4','5','6','7','8','9','0'];

	public function code($length = 6)
	{
		return implode('', $this->generator->randomElements(self::CHARS, $length));


		$size = strlen(self::CHARS) - 1;
		$chars = self::CHARS;
		$code = '';
		while($length--) {
			$code .= $chars[rand(0, $size)];
		}
		return $code;
	}
}