<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Applications;
use \Firebase\JWT\JWT;
use Illuminate\Http\Response;
use Validator;

class ApiAppAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authToken = $request->bearerToken();

        try {
            $this->payloadIsValid(
                $payload = (array) JWT::decode($authToken, ENV('API_AUTH_KEY'), ['HS256'])
            );
            $app = Applications::where('api_key', $payload['sub'])->firstOrFail();
        } catch (\Firebase\JWT\JWT\ExpiredException $e) {
            return response()->json(
                ['status' => false, 'errors' => ['authenticate' => 'Token expired']],
                Response::HTTP_UNAUTHORIZED
            );
        } catch (\Throwable $e) {
            return response()->json(
                ['status' => false, 'errors' => ['authenticate' => 'Token invalid']],
                Response::HTTP_UNAUTHORIZED
            );
        }

        $request->merge(['__authenticatedApp' => $app]);

        return $next($request);
    }

    /**
     * Check payload is valid
     * @param array $payload
     * @return boolean
     */
    private function payloadIsValid($payload)
    {
        $validator = Validator::make($payload, [
            'iss' => 'required|in:' . ENV('API_AUTH_ISS'),
            'sub' => 'required',
        ]);

        if (! $validator->passes()) {
            throw new \InvalidArgumentException;
            return false;
        }

        return true;
    }
}
