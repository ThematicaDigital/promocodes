<?php
namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller as Controller;
use Request as HttpRequest;

use \App\Models\Codes;
use \App\Models\Applications;
use \App\Models\Campaigns;
use \App\Models\Rewards;

use Faker\Generator as Faker;

use Validator;
use Carbon\Carbon;


/**
 * @resource Promocodes
 *
 * Promocodes
 */
class PromocodesController extends Controller
{
    /**
     * Method for generation a promocodes
     *
     * @api {post} /promocodes/generate Generation a promocodes
     * @apiDescription Request for generation a promocodes. You must use campaign_id or create new campaign
     * @apiName GenerationPromocodes
     * @apiGroup Promocodes
     * @apiVersion 1.0.0
     *
     * @apiParam {Number} campaign_id Campaign ID
     *
     * @apiHeader {String} Authorization Authorization value from /auth/app
     *
     * @apiSuccess (200) {Boolean} status Status request
     * @apiSuccess (200) {String} promocode Promocode
     *
     * @apiError {Boolean} status Status request
     * @apiError {String[]} [errors] Error messages
     *
     * @apiSampleRequest /promocodes/generate
     *
     * @apiHeaderExample {json} Request-Example:
     * {
     *      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJwcm9tb2NvZGVzIiwic3ViIjoia2V5IiwiaWF0IjoxNTA2NDk0NjUxLCJleHAiOjE1MDY1ODEwNTF9.-2WF__wHHenjes2p06jqUvS8F6UgLDqM6QLWSXiM6yA"
     * }
     *
     * @apiParamExample {json} Request-Example:
     * {
     *      "campaign_id": 12,
     * }
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *      "status": true,
     *      "promocode": "QWERTY6",
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 OK
     * {
     *      "status": false,
     *      "errors": {
     *          "campaign_id": [
     *              "Поле campaign_id обязательно для заполнения"
     *          ]
     *      }
     * }
     */
	public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'campaign_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['status' => false, 'errors' => $validator->errors()],
                Response::HTTP_BAD_REQUEST
            );
        }

        $campaign = Campaigns::where('id', $request->campaign_id)->first();

        if (!$campaign) {
            return response()->json(['status' => false, 'errors' => ['campaign_id' => 'Не найдено']], Response::HTTP_NOT_FOUND);
        }

        $faker = \Faker\Factory::create();

        $promocode = factory(Codes::class)->create([
            'generation_date' => date('Y-m-d'),
            'campaign_id' => $request->campaign_id,
            'status' => Codes::NO_ACTIVATION
        ]);

        if ($promocode) {
            return response()->json([
                'status' => true,
                'promocode' => $promocode->code
            ], Response::HTTP_OK);
        }

        return response()->json(['status' => false], Response::HTTP_NOT_FOUND);
    }

    /**
     * Method for activation a promocodes
     *
     * @api {put} /promocodes/activate Activation a promocodes
     * @apiDescription Request for activation a promocodes
     * @apiName ActivationPromocodes
     * @apiGroup Promocodes
     * @apiVersion 1.0.1
     *
     * @apiParam {String{2..8}="ios","android"} platform Device's platform
     * @apiParam {String{..64}} bundle_id Bundle ID of application
     * @apiParam {String{2..16}} promocode Promocode
     * @apiHeader {String} Authorization Authorization value from /auth/app
     *
     * @apiSuccess (200) {Boolean} status Status request
	 * @apiSuccess (200) {Number} count Count of rewards
     * @apiSuccess (200) {Object[]} rewards List of rewards
     * @apiSuccess (200) {Number} rewards.id Reward ID
     * @apiSuccess (200) {String} rewards.title Reward title
     * @apiSuccess (200) {String} rewards.reward Reward value
     *
     * @apiError {Boolean} status Status request
     * @apiError {String[]} [errors] Error messages
     *
     * @apiSampleRequest /promocodes/activate
     *
     * @apiHeaderExample {json} Request-Example:
     * {
     *      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJwcm9tb2NvZGVzIiwic3ViIjoia2V5IiwiaWF0IjoxNTA2NDk0NjUxLCJleHAiOjE1MDY1ODEwNTF9.-2WF__wHHenjes2p06jqUvS8F6UgLDqM6QLWSXiM6yA"
     * }
     *
     * @apiParamExample {json} Request-Example:
     * {
     * 		"bundle_id": "i.mult.parovozov",
     * 		"platform": "android",
     * 		"promocode": "QWERTY6"
     * }
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     * 		"status": true,
     * 		"count": 2,
     * 		"rewards": [
     * 			{
     * 				"id": 1,
     * 				"title": "Golden sword"
     *     			"reward": "golden_sword"
     * 			},
     * 			{
     * 				"id": 2,
     *     			"title": "Silver armor"
     * 				"reward": "silver_armor"
     * 			},
     * 		]
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 OK
     * {
     * 		"status": false,
     * 		"errors": {
     * 			"bundle_id": [
     * 				"Поле bundle id обязательно для заполнения."
     * 			]
     * 		}
     * }
     */
	public function put(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'bundle_id' => 'required|max:64',
            'platform' => 'required|in:ios,android|max:8',
            'promocode' => 'required|max:16',
        ]);

        if ($validator->fails()) {
        	return response()->json(
        		['status' => false, 'errors' => $validator->errors()],
        		Response::HTTP_BAD_REQUEST
    		);
        }

        $platform = Applications::encodePlatform($request->platform);
        if (!$platform) {
        	return response()->json(
        		['status' => false, 'errors' => ['platform' => 'Platform not found']],
        		Response::HTTP_NOT_FOUND
    		);
        }

        if ($platform == Applications::PLATFORM_ANDROID) {
            $application = Applications::where('bundle_android', $request->bundle_id)->first();
        } else {
            $application = Applications::where('bundle_ios', $request->bundle_id)->first();
        }

        if (!$application) {
        	return response()->json(
        		['status' => false, 'errors' => ['application' => ['Application not found']]],
        		Response::HTTP_NOT_FOUND
    		);
        }

        if (HttpRequest::header('If-Modified-Since')) {
        	$count = Applications::where('updated_at', '>', date('Y-m-d H:i:s', strtotime(HttpRequest::header('If-Modified-Since'))))->count();
        	if ($count == 0) {
        		return response()
	        		->json(['status' => true], Response::HTTP_NOT_MODIFIED)
	        		->header('Last-Modified', substr(gmdate('r', time()), 0, -5) . 'GMT');
        	}
        }

        $rewards = Rewards::select(
	        	'rewards.id',
                'rewards.reward',
	        	'rewards.title'
        	)
        	->leftJoin('campaigns_rewards', 'campaigns_rewards.reward_id', '=', 'rewards.id')
            ->leftJoin('campaigns', 'campaigns.id', '=', 'campaigns_rewards.campaign_id')
            ->leftJoin('applications_campaigns', 'applications_campaigns.campaign_id', '=', 'campaigns.id')
            ->leftJoin('applications', 'applications.id', '=', 'applications_campaigns.application_id')
        	->leftJoin('codes', 'codes.campaign_id', '=', 'campaigns.id')
            ->where(function ($query) {
                $query->where('campaigns.consumable', 0)
                    ->orWhere('codes.status', Codes::NO_ACTIVATION);
            })
        	->where(function ($query) {
                $query->whereNull('campaigns.end_date')
                    ->orWhere('campaigns.end_date', '>', Carbon::now()->toDateString());
            })
        	->where(function ($query) {
                $query->whereNull('campaigns.start_date')
                    ->orWhere('campaigns.start_date', '<=', Carbon::now()->toDateString());
            })
        	->where('applications.id', $application->id)
            ->where('codes.code', $request->promocode)
            ->whereNull('campaigns.deleted_at')
            ->whereNull('codes.deleted_at')
            ->whereNull('applications.deleted_at')
        	->whereNull('rewards.deleted_at')
        	->get();

        if (count($rewards) > 0) {

        	$promocode = Codes::where('code', $request->promocode)->first();
        	$promocode->activation_date = date('Y-m-d');
        	$promocode->status = Codes::ACTIVATION;
        	$promocode->save();

        	$json = [];
        	foreach ($rewards as $reward) {
        		$json[] = [
        			'id' => $reward->id,
                    'reward' => $reward->reward,
        			'title' => $reward->title,
        		];
        	}
        	return response()
				->json(['status' => true, 'count' => count($rewards), 'rewards' => $json], Response::HTTP_OK)
				->header('Last-Modified', substr(gmdate('r', time()), 0, -5) . 'GMT');
        }

        return response()->json(
        	['status' => false, 'errors' => ['rewards' => ['Rewards not found']]],
        	Response::HTTP_NOT_FOUND
    	);
    }

    /**
     * Method for verify a promocodes
     *
     * @api {get} /promocodes/verify Verify a promocodes
     * @apiDescription Request for verify a promocodes
     * @apiName VerifyPromocodes
     * @apiGroup Promocodes
     * @apiVersion 1.0.0
     *
     * @apiParam {String{2..16}} promocode Promocode
     * @apiHeader {String} Authorization Authorization value from /auth/app
     *
     * @apiSuccess (200) {Boolean} status Status request
     *
     * @apiError {Boolean} status Status request
     * @apiError {String[]} [errors] Error messages
     *
     * @apiSampleRequest /promocodes/verify
     *
     * @apiHeaderExample {json} Request-Example:
     * {
     *      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJwcm9tb2NvZGVzIiwic3ViIjoia2V5IiwiaWF0IjoxNTA2NDk0NjUxLCJleHAiOjE1MDY1ODEwNTF9.-2WF__wHHenjes2p06jqUvS8F6UgLDqM6QLWSXiM6yA"
     * }
     *
     * @apiParamExample {json} Request-Example:
     * {
     * 		"promocode": "QWERTY6"
     * }
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *      "status": true,
     *      "consumable": true,
     * 		"activated": false
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 OK
     * {
     * 		"status": false,
     * 		"errors": {
     * 			"promocode": [
     * 				"Promocode not found"
     * 			]
     * 		}
     * }
     */
	public function get(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'promocode' => 'required|max:16',
        ]);

        if ($validator->fails()) {
        	return response()->json(
        		['status' => false, 'errors' => $validator->errors()],
        		Response::HTTP_BAD_REQUEST
    		);
        }

        $promocode = Codes::where('code', $request->promocode)
        	->leftJoin('campaigns', 'campaigns.id', '=', 'codes.campaign_id')
            ->whereNull('campaigns.deleted_at')
        	->where(function ($query) {
                $query->whereNull('campaigns.end_date')
                    ->orWhere('campaigns.end_date', '>', Carbon::now()->toDateString());
            })
        	->where(function ($query) {
                $query->whereNull('campaigns.start_date')
                    ->orWhere('campaigns.start_date', '<=', Carbon::now()->toDateString());
            })
        	->first();

    	if ($promocode) {
            if ($promocode->campaign->consumable) {
                return response()->json([
                    'status' => true,
                    'consumable' => true,
                    'activated' => $promocode->activation_date ? true : false
                ], Response::HTTP_OK);
            }
            return response()->json([
                'status' => true,
                'consumable' => false,
                'activated' => false
            ], Response::HTTP_OK);

    	}

        return response()->json(['status' => false], Response::HTTP_NOT_FOUND);
    }
}