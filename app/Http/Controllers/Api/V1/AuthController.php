<?php
namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller as Controller;
use Request as HttpRequest;

use App\Models\Applications;
use Illuminate\Support\Str;


/**
 * @resource Auth
 *
 * Auth
 */
class AuthController extends Controller
{
	/**
     * Method for authentification a applications
     *
     * @api {post} /auth/app Auth a applications
     * @apiDescription Request for authentification a applications by Auth Basic Bearer. You should save the return value and use it in each requests in the future.
     *
     * @apiName AuthentificationApplications
     * @apiGroup Auth
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization value as base64('application_key:application_secret')
     *
     * @apiSuccess (200) {Boolean} status Status request
	 * @apiSuccess (200) {String} token_type Type of token
     * @apiSuccess (200) {String} access_token Auth token
     *
     * @apiError {Boolean} status Status request
     * @apiError {String[]} [errors] Error messages
     *
     * @apiSampleRequest /auth/app
     *
     * @apiHeaderExample {json} Request-Example:
     * {
     * 		"Authorization": "Basic a2V5OnNlY3JldA=="
     * }
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     * 		"status": true,
     * 		"token_type": "Bearer",
     * 		"access_token": ".eyJpc3MiOiJwcm9tb2NvZGVzIiwic3ViIjoia2V5IiwiaWF0IjoxNTA2NDk1MzYyLCJleHAiOjE1MDY1ODE3NjJ9.NzJmw0NVgvkjAPP2J5E6ocRaVsW2FRoDTK4nbwl5Cvk"
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 OK
     * {
     * 		"status": false,
     * 		"errors": {
     * 			"credentials": [
     * 				"Invalid credentials"
     * 			]
     * 		}
     * }
     */
	public function authenticate(Request $request)
	{
		$credentials = base64_decode(Str::substr($request->header('Authorization'), 6));

	    try {
	        list($appKey, $appSecret) = explode(':', $credentials);
	        $app = Applications::where('api_key', $appKey)->where('api_secret', $appSecret)->firstOrFail();
	    } catch (\Throwable $e) {
	    	return response()->json(
        		['status' => false, 'errors' => ['credentials' => 'Invalid credentials']],
        		Response::HTTP_UNAUTHORIZED
    		);
	    }

	    return response()->json(
    		['status' => true, 'token_type' => 'Bearer', 'access_token' => $app->generateAuthToken()],
    		Response::HTTP_OK
		);
	}
}