<?php
namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller as Controller;
use Request as HttpRequest;

use \App\Models\Campaigns;

use Faker\Generator as Faker;

use Validator;
use Carbon\Carbon;


/**
 * @resource Campaigns
 *
 * Campaigns
 */
class CampaignsController extends Controller
{
	/**
     * Method for create a campaigns
     *
     * @api {post} /campaigns Create a campaigns
     * @apiDescription Request for creating a campaigns
     * @apiName CreateCampaigns
     * @apiGroup Campaigns
     * @apiVersion 1.0.0
     *
     * @apiParam {String{2..64}} title Campaign title
     * @apiParam {Boolean} [consumable=false] Campaign consumable
     * @apiParam {String{10}} [start_date=NULL] Campaign start date Y-m-d
     * @apiParam {String{10}} [end_date=NULL] Campaign end date Y-m-d
     *
     * @apiHeader {String} Authorization Authorization value from /auth/app
     *
     * @apiSuccess (200) {Boolean} status Status request
     * @apiSuccess (200) {Object} campaign Campaign
     * @apiSuccess (200) {Number} campaign.id Campaign ID
     * @apiSuccess (200) {String} campaign.title Campaign title
     * @apiSuccess (200) {Boolean} campaign.consumable=false Campaign consumable
     * @apiSuccess (200) {String{10}} campaign.start_date=NULL Campaign start date Y-m-d
     * @apiSuccess (200) {String{10}} campaign.end_date=NULL Campaign end date Y-m-d
     *
     * @apiError {Boolean} status Status request
     * @apiError {String[]} [errors] Error messages
     *
     * @apiSampleRequest /campaigns
     *
     * @apiHeaderExample {json} Request-Example:
     * {
     *      "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJwcm9tb2NvZGVzIiwic3ViIjoia2V5IiwiaWF0IjoxNTA2NDk0NjUxLCJleHAiOjE1MDY1ODEwNTF9.-2WF__wHHenjes2p06jqUvS8F6UgLDqM6QLWSXiM6yA"
     * }
     *
     * @apiParamExample {json} Request-Example:
     * {
     *      "title": "Campaign title",
     *      "consumable": true,
     *      "start_date": "2017-01-01",
     *      "end_date": "2017-12-31",
     * }
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *      "status": true,
     *      "campaign": {
     *      	"title": "Campaign title",
     *       	"consumable": true,
     *        	"start_date": "2017-01-01",
     *        	"end_date": "2017-12-31",
     *      },
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 OK
     * {
     *      "status": false,
     *      "errors": {
     *          "title": [
     *              "Поле title обязательно для заполнения"
     *          ]
     *      }
     * }
     */
	public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:64|min:2',
            'consumable' => 'nullable',
            'start_date' => 'nullable|date_format:Y-m-d',
            'end_date' => 'nullable|date_format:Y-m-d',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['status' => false, 'errors' => $validator->errors()],
                Response::HTTP_BAD_REQUEST
            );
        }

        $faker = \Faker\Factory::create();

        $data = $request->all();
        if (!empty($data['consumable'])) {
            $data['consumable'] = filter_var($data['consumable'], FILTER_VALIDATE_BOOLEAN);
        }


        $campaign = Campaigns::create($data);

        if ($campaign) {
            return response()->json([
                'status' => true,
                'campaign' => [
                	'id' => $campaign->id,
                	'title' => $campaign->title,
                	'consumable' => (bool)$campaign->consumable,
                	'start_date' => $campaign->start_date,
                	'end_date' => $campaign->end_date,
                ]
            ], Response::HTTP_OK);
        }

        return response()->json(['status' => false], Response::HTTP_BAD_REQUEST);
    }
}