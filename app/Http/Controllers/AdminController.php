<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\Applications;
use App\Models\Campaigns;
use App\Models\Codes;

use Faker\Generator as Faker;
use Excel;
use Lava;
use Meta;
use Khill\Lavacharts\Lavacharts;

class AdminController extends Controller
{
    /**
     * Statistic action
     *
     * @return Response
     */
    public function statistics()
    {
        Meta::addJs('admin-statistics', asset('packages/sleepingowl/custom/statistics.js'));

    	$applications = \AdminFormElement::multiselect('applications', 'Приложения', Applications::class);
    	$campaigns = \AdminFormElement::multiselect('campaigns', 'Кампании', Campaigns::class);
    	$start_date = \AdminFormElement::date('start_date', 'Дата начала')->setFormat('Y-m-d')->setPickerFormat('Y-m-d');
        $end_date = \AdminFormElement::date('end_date', 'Дата окончания')->setFormat('Y-m-d')->setPickerFormat('Y-m-d')->setCurrentDate();
		$parametrs = \AdminFormElement::select('parametrs', 'Параметр')->setOptions([
            'activation_date' => 'Кол-во активаций',
            'generation_date' => 'Кол-во генераций',
        ])->setDefaultValue('activation_date');

        $lava = new Lavacharts;
        $data = $lava->DataTable();
        $data->addDateColumn('Дата')
             ->addNumberColumn('Кол-во активаций');
        $lava->LineChart('ActivationByDate', $data, [
            'events' => [
                'ready' => 'initStatistics'
            ],
            'pointSize' => 5,
            'height' => 500
        ]);

    	$content = view('admin.statistics', compact(
    		'applications',
    		'campaigns',
    		'start_date',
            'end_date',
    		'parametrs',
            'lava'
		));

    	return \AdminSection::view($content, 'Статистика');
    }

    public function chars(Request $request)
    {
        $title = $request->parametrs;
        if ($request->parametrs == 'activation_date') {
            $title = 'Кол-во активаций';
        } elseif ($request->parametrs == 'generation_date') {
            $title = 'Кол-во генераций';
        }

        $data = Lava::DataTable();
        $data->addDateColumn('Дата')
            ->addNumberColumn($title);

        $codes = Codes::select($request->parametrs, \DB::raw('COUNT(*) AS count'))
            ->groupBy($request->parametrs);

        if (!empty($request->campaigns)) {
            $codes = $codes->whereIn('codes.campaign_id', $request->campaigns);
        }

        if (!empty($request->applications)) {
            $codes->leftJoin('applications_campaigns', 'applications_campaigns.campaign_id', '=', 'codes.campaign_id')
                ->whereIn('applications_campaigns.application_id', $request->applications);
        }

        if (!empty($request->start_date)) {
            $codes = $codes->where($request->parametrs, '>=', $request->start_date);
        }

        if (!empty($request->end_date)) {
            $codes = $codes->where($request->parametrs, '<=', $request->end_date);
        }

        $codes = $codes->get();
        foreach ($codes as $i => $code) {
            $data->addRow([
                $code->{$request->parametrs},
                $code->count
            ]);
        }

        return $data->toJson();
    }

    /**
     * Generate new promocodes action
     *
     * @return Response
     */
    public function generateCodes(Request $request)
    {
        $faker = \Faker\Factory::create();

        factory(\App\Models\Codes::class, (int)$request->count)->create([
            'generation_date' => date('Y-m-d'),
            'campaign_id' => $request->campaign_id,
            'status' => Codes::NO_ACTIVATION
        ]);

        return response()->json([
            'status' => true,
            'title' => 'Успешно',
            'message' => 'Промокоды успешно сгенерированны',
        ], Response::HTTP_OK);
    }

    /**
     * Import new promocodes action
     *
     * @return Response
     */
    public function importCodes(Request $request)
    {
        $extension = $request->file('upload-codes')->getClientOriginalExtension();
        $csvFile = md5(time()) . '.' . $extension;
        $path = base_path() . '/public/csv/uploads/';

        $request->file('upload-codes')->move($path, $csvFile);

        Excel::load($path . $csvFile)->each(function ($csvLine) use ($request) {
            factory(\App\Models\Codes::class)->create([
                'generation_date' => date('Y-m-d'),
                'campaign_id' => $request->campaign_id,
                'status' => Codes::NO_ACTIVATION,
                'code' => current($csvLine->toArray())
            ]);
        });

        return response()->json([
            'status' => true,
            'title' => 'Успешно',
            'message' => 'Промокоды успешно импортированны',
        ], Response::HTTP_OK);
    }

    /**
     * Export promocodes action
     *
     * @return Response
     */
    public function exportCodes(Request $request)
    {
        if ($request->campaign_id) {
            $codes = Codes::where('campaign_id', $request->campaign_id)->orderBy('id', 'desc')->get()->pluck('code')->chunk(1);
        } else {
            $codes = Codes::orderBy('id', 'desc')->get()->pluck('code')->chunk(1);
        }

        return Excel::create('promocodes', function($excel) use ($codes) {
            $excel->sheet('Sheet', function($sheet) use ($codes)
            {
                $sheet->fromArray($codes);
            });
        })->download('csv');
    }
}