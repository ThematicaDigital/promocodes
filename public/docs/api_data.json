[
  {
    "type": "post",
    "url": "/auth/app",
    "title": "Auth a applications",
    "description": "<p>Request for authentification a applications by Auth Basic Bearer. You should save the return value and use it in each requests in the future.</p>",
    "name": "AuthentificationApplications",
    "group": "Auth",
    "version": "1.0.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Authorization value as base64('application_key:application_secret')</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n\t\t\"Authorization\": \"Basic a2V5OnNlY3JldA==\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "token_type",
            "description": "<p>Type of token</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>Auth token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\t\t\"status\": true,\n\t\t\"token_type\": \"Bearer\",\n\t\t\"access_token\": \".eyJpc3MiOiJwcm9tb2NvZGVzIiwic3ViIjoia2V5IiwiaWF0IjoxNTA2NDk1MzYyLCJleHAiOjE1MDY1ODE3NjJ9.NzJmw0NVgvkjAPP2J5E6ocRaVsW2FRoDTK4nbwl5Cvk\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String[]",
            "optional": true,
            "field": "errors",
            "description": "<p>Error messages</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 OK\n{\n\t\t\"status\": false,\n\t\t\"errors\": {\n\t\t\t\"credentials\": [\n\t\t\t\t\"Invalid credentials\"\n\t\t\t]\n\t\t}\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8000/api/v1/auth/app"
      }
    ],
    "filename": "app/Http/Controllers/Api/V1/AuthController.php",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/campaigns",
    "title": "Create a campaigns",
    "description": "<p>Request for creating a campaigns</p>",
    "name": "CreateCampaigns",
    "group": "Campaigns",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "2..64",
            "optional": false,
            "field": "title",
            "description": "<p>Campaign title</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "consumable",
            "defaultValue": "false",
            "description": "<p>Campaign consumable</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "10",
            "optional": true,
            "field": "start_date",
            "defaultValue": "NULL",
            "description": "<p>Campaign start date Y-m-d</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "10",
            "optional": true,
            "field": "end_date",
            "defaultValue": "NULL",
            "description": "<p>Campaign end date Y-m-d</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n     \"title\": \"Campaign title\",\n     \"consumable\": true,\n     \"start_date\": \"2017-01-01\",\n     \"end_date\": \"2017-12-31\",\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Authorization value from /auth/app</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n     \"Authorization\": \"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJwcm9tb2NvZGVzIiwic3ViIjoia2V5IiwiaWF0IjoxNTA2NDk0NjUxLCJleHAiOjE1MDY1ODEwNTF9.-2WF__wHHenjes2p06jqUvS8F6UgLDqM6QLWSXiM6yA\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "campaign",
            "description": "<p>Campaign</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "campaign.id",
            "description": "<p>Campaign ID</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "campaign.title",
            "description": "<p>Campaign title</p>"
          },
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "campaign.consumable",
            "defaultValue": "false",
            "description": "<p>Campaign consumable</p>"
          },
          {
            "group": "200",
            "type": "String",
            "size": "10",
            "optional": false,
            "field": "campaign.start_date",
            "defaultValue": "NULL",
            "description": "<p>Campaign start date Y-m-d</p>"
          },
          {
            "group": "200",
            "type": "String",
            "size": "10",
            "optional": false,
            "field": "campaign.end_date",
            "defaultValue": "NULL",
            "description": "<p>Campaign end date Y-m-d</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"status\": true,\n     \"campaign\": {\n     \t\"title\": \"Campaign title\",\n      \t\"consumable\": true,\n       \t\"start_date\": \"2017-01-01\",\n       \t\"end_date\": \"2017-12-31\",\n     },\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String[]",
            "optional": true,
            "field": "errors",
            "description": "<p>Error messages</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 OK\n{\n     \"status\": false,\n     \"errors\": {\n         \"title\": [\n             \"Поле title обязательно для заполнения\"\n         ]\n     }\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8000/api/v1/campaigns"
      }
    ],
    "filename": "app/Http/Controllers/Api/V1/CampaignsController.php",
    "groupTitle": "Campaigns"
  },
  {
    "type": "put",
    "url": "/promocodes/activate",
    "title": "Activation a promocodes",
    "description": "<p>Request for activation a promocodes</p>",
    "name": "ActivationPromocodes",
    "group": "Promocodes",
    "version": "1.0.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "2..8",
            "allowedValues": [
              "\"ios\"",
              "\"android\""
            ],
            "optional": false,
            "field": "platform",
            "description": "<p>Device's platform</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "..64",
            "optional": false,
            "field": "bundle_id",
            "description": "<p>Bundle ID of application</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "2..16",
            "optional": false,
            "field": "promocode",
            "description": "<p>Promocode</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n\t\t\"bundle_id\": \"i.mult.parovozov\",\n\t\t\"platform\": \"android\",\n\t\t\"promocode\": \"QWERTY6\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Authorization value from /auth/app</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n     \"Authorization\": \"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJwcm9tb2NvZGVzIiwic3ViIjoia2V5IiwiaWF0IjoxNTA2NDk0NjUxLCJleHAiOjE1MDY1ODEwNTF9.-2WF__wHHenjes2p06jqUvS8F6UgLDqM6QLWSXiM6yA\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>Count of rewards</p>"
          },
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "rewards",
            "description": "<p>List of rewards</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "rewards.id",
            "description": "<p>Reward ID</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "rewards.title",
            "description": "<p>Reward title</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "rewards.reward",
            "description": "<p>Reward value</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\t\t\"status\": true,\n\t\t\"count\": 2,\n\t\t\"rewards\": [\n\t\t\t{\n\t\t\t\t\"id\": 1,\n\t\t\t\t\"title\": \"Golden sword\"\n    \t\t\t\"reward\": \"golden_sword\"\n\t\t\t},\n\t\t\t{\n\t\t\t\t\"id\": 2,\n    \t\t\t\"title\": \"Silver armor\"\n\t\t\t\t\"reward\": \"silver_armor\"\n\t\t\t},\n\t\t]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String[]",
            "optional": true,
            "field": "errors",
            "description": "<p>Error messages</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 OK\n{\n\t\t\"status\": false,\n\t\t\"errors\": {\n\t\t\t\"bundle_id\": [\n\t\t\t\t\"Поле bundle id обязательно для заполнения.\"\n\t\t\t]\n\t\t}\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8000/api/v1/promocodes/activate"
      }
    ],
    "filename": "app/Http/Controllers/Api/V1/PromocodesController.php",
    "groupTitle": "Promocodes"
  },
  {
    "type": "post",
    "url": "/promocodes/generate",
    "title": "Generation a promocodes",
    "description": "<p>Request for generation a promocodes. You must use campaign_id or create new campaign</p>",
    "name": "GenerationPromocodes",
    "group": "Promocodes",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "campaign_id",
            "description": "<p>Campaign ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n     \"campaign_id\": 12,\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Authorization value from /auth/app</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n     \"Authorization\": \"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJwcm9tb2NvZGVzIiwic3ViIjoia2V5IiwiaWF0IjoxNTA2NDk0NjUxLCJleHAiOjE1MDY1ODEwNTF9.-2WF__wHHenjes2p06jqUvS8F6UgLDqM6QLWSXiM6yA\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "promocode",
            "description": "<p>Promocode</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"status\": true,\n     \"promocode\": \"QWERTY6\",\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String[]",
            "optional": true,
            "field": "errors",
            "description": "<p>Error messages</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 OK\n{\n     \"status\": false,\n     \"errors\": {\n         \"campaign_id\": [\n             \"Поле campaign_id обязательно для заполнения\"\n         ]\n     }\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8000/api/v1/promocodes/generate"
      }
    ],
    "filename": "app/Http/Controllers/Api/V1/PromocodesController.php",
    "groupTitle": "Promocodes"
  },
  {
    "type": "get",
    "url": "/promocodes/verify",
    "title": "Verify a promocodes",
    "description": "<p>Request for verify a promocodes</p>",
    "name": "VerifyPromocodes",
    "group": "Promocodes",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "2..16",
            "optional": false,
            "field": "promocode",
            "description": "<p>Promocode</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n\t\t\"promocode\": \"QWERTY6\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Authorization value from /auth/app</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n     \"Authorization\": \"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJwcm9tb2NvZGVzIiwic3ViIjoia2V5IiwiaWF0IjoxNTA2NDk0NjUxLCJleHAiOjE1MDY1ODEwNTF9.-2WF__wHHenjes2p06jqUvS8F6UgLDqM6QLWSXiM6yA\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"status\": true,\n     \"consumable\": true,\n\t\t\"activated\": false\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Status request</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String[]",
            "optional": true,
            "field": "errors",
            "description": "<p>Error messages</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 OK\n{\n\t\t\"status\": false,\n\t\t\"errors\": {\n\t\t\t\"promocode\": [\n\t\t\t\t\"Promocode not found\"\n\t\t\t]\n\t\t}\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://127.0.0.1:8000/api/v1/promocodes/verify"
      }
    ],
    "filename": "app/Http/Controllers/Api/V1/PromocodesController.php",
    "groupTitle": "Promocodes"
  }
]
