let ready = false;
function initStatistics()
{
	if (!ready) {
		$.ajax({
			type: 'GET',
	      	url: $('#filters-statistics').attr('action'),
	      	dataType: 'json',
	      	data: $('#filters-statistics').serialize(),
		  	success: function(dataTableJson) {
		  		lava.loadData('ActivationByDate', dataTableJson);
		  		ready = true;
		  	},
		  	beforeSend: function() {
	      		$('#filters-statistics button').prop('disabled', true).find('i').removeClass('fa-filter').addClass('fa-spinner fa-spin');
	      	},
	      	complete: function() {
	      		$('#filters-statistics button').prop('disabled', false).find('i').addClass('fa-filter').removeClass('fa-spinner fa-spin');
	      	}
	  	});
	}
}

$(document).on('submit', '#filters-statistics', function(event) {
	event.preventDefault();
	ready = false;
	initStatistics();
	return false;
});