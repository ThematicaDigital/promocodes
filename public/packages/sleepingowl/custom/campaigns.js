/**
 * Generate codes
 */
$(document).on('click', '#generate-promocodes .generate-codes', function(event) {
	var campaign_id = $(this).data('campaign_id');
	var count = $('[name="count-generate-codes"]').val();
	var button = $(this);
	if (count > 0) {
		$.ajax({
			type: 'POST',
	      	url: '/admin/promocodes/generate-codes',
	      	dataType: 'json',
	      	data: {
	      		campaign_id: campaign_id,
	      		count: count
	      	},
		  	success: function(response) {
		  		if (response.message && response.title) {
		  			Admin.Messages.success(response.title, response.message);
		  		}
		  		if (response.status == true) {
		  			setTimeout(function() {
					  location.reload();
					}, 2000);
		  		}
		  	},
		  	beforeSend: function() {
		  		$('[name="count-generate-codes"]').prop('disabled', true);
	      		button.prop('disabled', true).find('i').removeClass('fa-cube').addClass('fa-spinner fa-spin');
	      	},
	      	complete: function() {
	      		$('[name="count-generate-codes"]').prop('disabled', false);
	      		button.prop('disabled', false).find('i').addClass('fa-cube').removeClass('fa-spinner fa-spin');
	      	}
	  	});
	} else {
		Admin.Messages.error('Ошибка!', 'Неверное количество промокодов для генерации');
	}
});

/**
 * Import codes
 */
$(document).on('click', '#generate-promocodes .import-codes', function(event) {
	$(this).find('i').toggleClass('fa-upload').toggleClass('fa-spinner fa-spin');
	$('#upload-codes').trigger('click');
	document.body.onfocus = function(){
		if (!$('#generate-promocodes #upload-codes').val().length) {
			$('#generate-promocodes .import-codes')
	      		.prop('disabled', false)
	      		.find('i')
	      		.addClass('fa-upload')
	      		.removeClass('fa-spinner fa-spin');
		}
        document.body.onfocus = null
	};
});
$(document).on('change', '#generate-promocodes #upload-codes', function(event) {
	console.log($(this).val());
	if ($(this).val() != '') {
		var campaign_id = $('#generate-promocodes .import-codes').data('campaign_id');
		var formData = new FormData();
		formData.append("upload-codes", $('#upload-codes').prop('files')[0]);
		$.ajax({
			type: 'POST',
	      	url: '/admin/promocodes/import-codes/' + campaign_id,
	      	dataType: 'json',
	      	data: formData,
			mimeTypes: 'multipart/form-data',
	        processData: false,
	        contentType: false,
		  	success: function(response) {
		  		if (response.message && response.title) {
		  			Admin.Messages.success(response.title, response.message);
		  		}
		  		setTimeout(function() {
				  location.reload();
				}, 2000);
		  	},
		  	beforeSend: function() {
	      		$('#generate-promocodes .import-codes')
		      		.prop('disabled', true)
		      		.find('i')
		      		.removeClass('fa-upload')
		      		.addClass('fa-spinner fa-spin');
	      	},
	      	complete: function() {
	      		$('#generate-promocodes .import-codes')
		      		.prop('disabled', false)
		      		.find('i')
		      		.addClass('fa-upload')
		      		.removeClass('fa-spinner fa-spin');
	      	}
	  	});
	} else {
		$('#generate-promocodes .import-codes')
      		.prop('disabled', false)
      		.find('i')
      		.addClass('fa-upload')
      		.removeClass('fa-spinner fa-spin');
	}
});