/**
 * Generate codes
 */
function generate($length = 12)
{
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for (var i = 0; i < $length; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	return text;
}

/**
 * Generate codes
 */
$(document).on('click', '[rel="generate"]', function(event) {
	$(this).parents('.input-group').find('input').val(generate());
	return true;
});