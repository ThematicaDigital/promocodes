<?php

use Illuminate\Database\Seeder;
use App\Models\Users;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Users::truncate();
        factory(Users::class, 19)->create();
        factory(Users::class)->create([
        	'email' => 'admin@site.com',
        	'password' => bcrypt('password')
    	]);
    }
}
