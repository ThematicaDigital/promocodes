<?php

use Illuminate\Database\Seeder;
use App\Models\Campaigns;
use App\Models\Codes;

class CodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Codes::truncate();
        factory(Codes::class, 100)->create();
    }
}
