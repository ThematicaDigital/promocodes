<?php

use Illuminate\Database\Seeder;
use App\Models\Applications;

class ApplicationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Applications::truncate();
        factory(Applications::class, 20)->create();
    }
}
