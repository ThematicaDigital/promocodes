<?php

use Illuminate\Database\Seeder;
use App\Models\Rewards;
use App\Models\Campaigns;

class RewardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rewards::truncate();
        DB::table('campaigns_rewards')->truncate();
        factory(Rewards::class, 20)->create()->each(function($reward) {
            $reward->campaigns()->attach(Campaigns::inRandomOrder()->limit(3)->get());
        });
    }
}
