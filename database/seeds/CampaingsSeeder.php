<?php

use Illuminate\Database\Seeder;
use App\Models\Campaigns;
use App\Models\Applications;

class CampaingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Campaigns::truncate();
        DB::table('applications_campaigns')->truncate();
        factory(Campaigns::class, 20)->create()->each(function($campaign) {
            $campaign->applications()->attach(Applications::inRandomOrder()->limit(3)->get());
        });
    }

}
