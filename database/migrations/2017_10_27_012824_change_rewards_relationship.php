<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRewardsRelationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('applications_rewards');

        if(!Schema::hasTable('campaigns_rewards')) {
            Schema::create('campaigns_rewards', function (Blueprint $table) {
                $table->integer('campaign_id')->unsigned();
                $table->integer('reward_id')->unsigned();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns_rewards');

        if(!Schema::hasTable('applications_rewards')) {
            Schema::create('applications_rewards', function (Blueprint $table) {
                $table->integer('application_id')->unsigned();
                $table->integer('reward_id')->unsigned();
            });
        }
    }
}
