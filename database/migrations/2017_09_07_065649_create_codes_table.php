<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use \App\Models\Codes;

class CreateCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('codes')) {
            Schema::create('codes', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code');
                $table->date('generation_date');
                $table->date('activation_date')->nullable()->default(null);
                $table->enum('status', [Codes::ACTIVATION, Codes::NO_ACTIVATION])->default(Codes::NO_ACTIVATION);
                $table->integer('campaign_id')->unsigned();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codes');
    }
}
