<?php

use Faker\Generator as Faker;
use App\Models\Rewards;

$factory->define(Rewards::class, function (Faker $faker) {
    return [
        'reward' => $faker->word(),
        'title' => $faker->word(),
    ];
});
