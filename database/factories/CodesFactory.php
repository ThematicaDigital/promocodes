<?php

use Faker\Generator as Faker;
use App\Models\Codes;
use App\Models\Campaigns;

$factory->define(Codes::class, function (Faker $faker) {
	$faker->addProvider(new \App\Providers\Faker\Codes($faker));
    return [
        'code' => $faker->code(),
        'status' => $faker->randomElement([Codes::NO_ACTIVATION]),
        // 'generation_date' => $faker->date('2017-m-d'),
        // 'activation_date' => $faker->date('2017-m-d'),
        // 'campaign_id' => Campaigns::inRandomOrder()->first()->id,
    ];
    /*return [
        'code' => $faker->code(),
        'status' => $faker->randomElement([Codes::ACTIVATION, Codes::NO_ACTIVATION]),
        'generation_date' => $faker->date('Y-m-d'),
        'campaign_id' => Campaigns::inRandomOrder()->first()->id,
    ];*/
});
