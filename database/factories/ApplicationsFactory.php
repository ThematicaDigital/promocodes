<?php

use Faker\Generator as Faker;
use App\Models\Applications;

$factory->define(Applications::class, function (Faker $faker) {
    return [
        'title' => $faker->word(),
        'bundle_android' => $faker->word(),
        'bundle_ios' => $faker->word(),
        'api_key' => uniqid(),
        'api_secret' => uniqid(),
    ];
});
