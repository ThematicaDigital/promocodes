<?php

use Faker\Generator as Faker;
use App\Models\Campaigns;

$factory->define(Campaigns::class, function (Faker $faker) {
    // return [
    //     'title' => $faker->word(),
    //     'consumable' => false,
    //     'start_date' => null,
    //     'end_date' => null,
    // ];
    return [
        'title' => $faker->word(),
        'consumable' => false,
        'start_date' => $faker->date('2017-m-d'),
        'end_date' => $faker->date('2017-m-d'),
    ];
});
